import psycopg2
import sys
from PyQt5.QtWidgets import (QApplication, QWidget,
    QTabWidget, QAbstractScrollArea,
    QVBoxLayout, QHBoxLayout,
    QTableWidget, QGroupBox,
    QTableWidgetItem, QPushButton, QMessageBox)

class MainWindow(QWidget):
    def __init__(self):
        super(MainWindow, self).__init__()
        self._connect_to_db()
        self.setWindowTitle("Shedule")
        self.vbox = QVBoxLayout(self)
        self.tabs = QTabWidget(self)
        self.vbox.addWidget(self.tabs)
        self._create_shedule_tab()


    def _connect_to_db(self):
        self.conn = psycopg2.connect(database="postgres",
                            user="postgres",
                            password="12341234",
                            host="localhost",
                            port="5432")
        self.cursor = self.conn.cursor()

    def _create_shedule_tab(self):
        self.shedule_tab = QWidget()
        self.tabs.addTab(self.shedule_tab, "Schedule")
        self.monday_gbox = QGroupBox("Monday")
        self.svbox = QVBoxLayout()
        self.shbox1 = QHBoxLayout()
        self.shbox2 = QHBoxLayout()
        self.svbox.addLayout(self.shbox1)
        self.svbox.addLayout(self.shbox2)
        self.shbox1.addWidget(self.monday_gbox)
        self._create_monday_table()

        self.tuesday_gbox = QGroupBox("Tuesday")
        self.layout_tuesday = QVBoxLayout()
        self.layout_tuesday1 = QHBoxLayout()
        self.layout_tuesday2 = QHBoxLayout()
        self.svbox.addLayout(self.layout_tuesday1)
        self.svbox.addLayout(self.layout_tuesday2)
        self.layout_tuesday1.addWidget(self.tuesday_gbox)
        self._create_tuesday_table()

        self.wednesday_gbox = QGroupBox("Wednesday")
        self.layout_wednesday = QVBoxLayout()
        self.layout_wednesday1 = QHBoxLayout()
        self.layout_wednesday2 = QHBoxLayout()
        self.svbox.addLayout(self.layout_wednesday1)
        self.svbox.addLayout(self.layout_wednesday2)
        self.layout_wednesday1.addWidget(self.wednesday_gbox)
        self._create_wednesday_table()

        self.thursday_gbox = QGroupBox("Thursday")
        self.layout_thursday = QVBoxLayout()
        self.layout_thursday1 = QHBoxLayout()
        self.layout_thursday2 = QHBoxLayout()
        self.svbox.addLayout(self.layout_thursday1)
        self.svbox.addLayout(self.layout_thursday2)
        self.layout_thursday1.addWidget(self.thursday_gbox)
        self._create_thursday_table()

        self.friday_gbox = QGroupBox("Friday")
        self.layout_friday = QVBoxLayout()
        self.layout_friday1 = QHBoxLayout()
        self.layout_friday2 = QHBoxLayout()
        self.svbox.addLayout(self.layout_friday1)
        self.svbox.addLayout(self.layout_friday2)
        self.layout_friday1.addWidget(self.friday_gbox)
        self._create_friday_table()

        self.saturday_gbox = QGroupBox("Saturday")
        self.layout_saturday = QVBoxLayout()
        self.layout_saturday1 = QHBoxLayout()
        self.layout_saturday2 = QHBoxLayout()
        self.svbox.addLayout(self.layout_saturday1)
        self.svbox.addLayout(self.layout_saturday2)
        self.layout_saturday1.addWidget(self.saturday_gbox)
        self._create_saturday_table()

        self.update_shedule_button = QPushButton("Update")
        self.svbox.addWidget(self.update_shedule_button)
        self.update_shedule_button.clicked.connect(self._update_shedule)
        self.shedule_tab.setLayout(self.svbox)

    def _create_monday_table(self):
        self.monday_table = QTableWidget()
        self.monday_table.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.monday_table.setColumnCount(4)
        self.monday_table.setHorizontalHeaderLabels(["Subject", "Time", "Room", ""])
        self._update_monday_table()
        self.mvbox = QVBoxLayout()
        self.mvbox.addWidget(self.monday_table)
        self.monday_gbox.setLayout(self.mvbox)

    def _create_tuesday_table(self):
        self.tuesday_table = QTableWidget()
        self.tuesday_table.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.tuesday_table.setColumnCount(4)
        self.tuesday_table.setHorizontalHeaderLabels(["Subject", "Time", "Room", ""])
        self._update_tuesday_table()
        self.mvbox = QVBoxLayout()
        self.mvbox.addWidget(self.tuesday_table)
        self.tuesday_gbox.setLayout(self.mvbox)

    def _create_wednesday_table(self):
        self.wednesday_table = QTableWidget()
        self.wednesday_table.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.wednesday_table.setColumnCount(4)
        self.wednesday_table.setHorizontalHeaderLabels(["Subject", "Time", "Room", ""])
        self._update_wednesday_table()
        self.mvbox = QVBoxLayout()
        self.mvbox.addWidget(self.wednesday_table)
        self.wednesday_gbox.setLayout(self.mvbox)

    def _create_thursday_table(self):
        self.thursday_table = QTableWidget()
        self.thursday_table.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.thursday_table.setColumnCount(4)
        self.thursday_table.setHorizontalHeaderLabels(["Subject", "Time", "Room", ""])
        self._update_thursday_table()
        self.mvbox = QVBoxLayout()
        self.mvbox.addWidget(self.thursday_table)
        self.thursday_gbox.setLayout(self.mvbox)

    def _create_friday_table(self):
        self.friday_table = QTableWidget()
        self.friday_table.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.friday_table.setColumnCount(4)
        self.friday_table.setHorizontalHeaderLabels(["Subject", "Time", "Room", ""])
        self._update_friday_table()
        self.mvbox = QVBoxLayout()
        self.mvbox.addWidget(self.friday_table)
        self.friday_gbox.setLayout(self.mvbox)

    def _create_saturday_table(self):
        self.saturday_table = QTableWidget()
        self.saturday_table.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.saturday_table.setColumnCount(4)
        self.saturday_table.setHorizontalHeaderLabels(["Subject", "Time", "Room", ""])
        self._update_saturday_table()
        self.mvbox = QVBoxLayout()
        self.mvbox.addWidget(self.saturday_table)
        self.saturday_gbox.setLayout(self.mvbox)



    def _update_monday_table(self):
        self.cursor.execute("SELECT * FROM timetable WHERE day_='Понедельник'")

        records = list(self.cursor.fetchall())
        self.monday_table.setRowCount(len(records) + 1)

        for i, r in enumerate(records):
            r = list(r)
            joinButton = QPushButton("Join")
            self.monday_table.setItem(i, 0,
                                      QTableWidgetItem(str(r[2])))
            self.monday_table.setItem(i, 1,
                                      QTableWidgetItem(str(r[4])))
            self.monday_table.setItem(i, 2,
                                      QTableWidgetItem(str(r[3])))
            self.monday_table.setCellWidget(i, 3, joinButton)
            joinButton.clicked.connect(lambda ch, num=i:
                                       self._change_day_from_table1(num))
            self.monday_table.resizeRowsToContents()

    def _update_tuesday_table(self):
        self.cursor.execute("SELECT * FROM timetable WHERE day_='Вторник'")

        records = list(self.cursor.fetchall())

        self.tuesday_table.setRowCount(len(records) + 1)
        for i, r in enumerate(records):
            r = list(r)
            joinButton = QPushButton("Join")
            self.tuesday_table.setItem(i, 0,
                                      QTableWidgetItem(str(r[2])))
            self.tuesday_table.setItem(i, 1,
                                      QTableWidgetItem(str(r[4])))
            self.tuesday_table.setItem(i, 2,
                                      QTableWidgetItem(str(r[3])))
            self.tuesday_table.setCellWidget(i, 3, joinButton)
            joinButton.clicked.connect(lambda ch, num=i:
                                       self._change_day_from_table2(num))
            self.tuesday_table.resizeRowsToContents()

    def _update_wednesday_table(self):
        self.cursor.execute("SELECT * FROM timetable WHERE day_='Среда'")

        records = list(self.cursor.fetchall())

        self.wednesday_table.setRowCount(len(records) + 1)
        for i, r in enumerate(records):
            r = list(r)
            joinButton = QPushButton("Join")
            self.wednesday_table.setItem(i, 0,
                                      QTableWidgetItem(str(r[2])))
            self.wednesday_table.setItem(i, 1,
                                      QTableWidgetItem(str(r[4])))
            self.wednesday_table.setItem(i, 2,
                                      QTableWidgetItem(str(r[3])))
            self.wednesday_table.setCellWidget(i, 3, joinButton)
            joinButton.clicked.connect(lambda ch, num=i:
                                       self._change_day_from_table3(num))
            self.wednesday_table.resizeRowsToContents()

    def _update_thursday_table(self):
        self.cursor.execute("SELECT * FROM timetable WHERE day_='Четверг'")

        records = list(self.cursor.fetchall())

        self.thursday_table.setRowCount(len(records) + 1)
        for i, r in enumerate(records):
            r = list(r)
            joinButton = QPushButton("Join")
            self.thursday_table.setItem(i, 0, QTableWidgetItem(str(r[2])))
            self.thursday_table.setItem(i, 1, QTableWidgetItem(str(r[4])))
            self.thursday_table.setItem(i, 2, QTableWidgetItem(str(r[3])))
            self.thursday_table.setCellWidget(i, 3, joinButton)
            joinButton.clicked.connect(lambda ch, num=i:
                                       self._change_day_from_table4(num))
            self.thursday_table.resizeRowsToContents()

    def _update_friday_table(self):
        self.cursor.execute("SELECT * FROM timetable WHERE day_='Пятница'")

        records = list(self.cursor.fetchall())

        self.friday_table.setRowCount(len(records) + 1)
        for i, r in enumerate(records):
            r = list(r)
            joinButton = QPushButton("Join")
            self.friday_table.setItem(i, 0,
                                      QTableWidgetItem(str(r[2])))
            self.friday_table.setItem(i, 1,
                                      QTableWidgetItem(str(r[4])))
            self.friday_table.setItem(i, 2,
                                      QTableWidgetItem(str(r[3])))
            self.friday_table.setCellWidget(i, 3, joinButton)
            joinButton.clicked.connect(lambda ch, num=i:
                                       self._change_day_from_table5(num))
            self.friday_table.resizeRowsToContents()

    def _update_saturday_table(self):
        self.cursor.execute("SELECT * FROM timetable WHERE day_='Суббота'")

        records = list(self.cursor.fetchall())

        self.saturday_table.setRowCount(len(records) + 1)
        for i, r in enumerate(records):
            r = list(r)
            joinButton = QPushButton("Join")
            self.saturday_table.setItem(i, 0,
                                      QTableWidgetItem(str(r[2])))
            self.saturday_table.setItem(i, 1,
                                      QTableWidgetItem(str(r[4])))
            self.saturday_table.setItem(i, 2,
                                      QTableWidgetItem(str(r[3])))
            self.saturday_table.setCellWidget(i, 3, joinButton)
            joinButton.clicked.connect(lambda ch, num=i:
                                       self._change_day_from_table6(num))
            self.saturday_table.resizeRowsToContents()


    def _change_day_from_table1(self, rowNum):
        row = list()
        for i in range(self.monday_table.columnCount()):
            try:
                row.append(self.monday_table.item(rowNum, i).text())
            except:
                row.append(None)
            try:
                self.cursor.execute("UPDATE timetable SET subject1='%s', steart_time='%s', room_numb='%s' WHERE day_='Понедельник' AND id='%s'", (row[2], row[4],row[3],row[0]))
                self.conn.commit()
            except:
                QMessageBox.about(self, "Error", "Enter all fields")

    def _change_day_from_table2(self, rowNum):
        row = list()
        for i in range(self.monday_table.columnCount()):
            try:
                row.append(self.monday_table.item(rowNum, i).text())
            except:
                row.append(None)
            try:
                self.cursor.execute("UPDATE timetable SET subject1='%s', steart_time='%s', room_numb='%s' WHERE day_='Вторник' AND id='%s'", (row[2], row[4],row[3],row[0]))
                self.conn.commit()
            except:
                QMessageBox.about(self, "Error", "Enter all fields")

    def _change_day_from_table3(self, rowNum):
        row = list()
        for i in range(self.monday_table.columnCount()):
            try:
                row.append(self.monday_table.item(rowNum, i).text())
            except:
                row.append(None)
            try:
                self.cursor.execute("UPDATE timetable SET subject1='%s', steart_time='%s', room_numb='%s' WHERE day_='Среда' AND id='%s'", (row[2], row[4],row[3],row[0]))
                self.conn.commit()
            except:
                QMessageBox.about(self, "Error", "Enter all fields")

    def _change_day_from_table4(self, rowNum):
        row = list()
        for i in range(self.monday_table.columnCount()):
            try:
                row.append(self.monday_table.item(rowNum, i).text())
            except:
                row.append(None)
            try:
                self.cursor.execute("UPDATE timetable SET subject1='%s', steart_time='%s', room_numb='%s' WHERE day_='Четверг' AND id='%s'", (row[2], row[4],row[3],row[0]))
                self.conn.commit()
            except:
                QMessageBox.about(self, "Error", "Enter all fields")

    def _change_day_from_table5(self, rowNum):
        row = list()
        for i in range(self.monday_table.columnCount()):
            try:
                row.append(self.monday_table.item(rowNum, i).text())
            except:
                row.append(None)
            try:
                self.cursor.execute("UPDATE timetable SET subject1='%s', steart_time='%s', room_numb='%s' WHERE day_='Пятница' AND id='%s'", (row[2], row[4],row[3],row[0]))
                self.conn.commit()
            except:
                QMessageBox.about(self, "Error", "Enter all fields")

    def _change_day_from_table6(self, rowNum):
        row = list()
        for i in range(self.monday_table.columnCount()):
            try:
                row.append(self.monday_table.item(rowNum, i).text())
            except:
                row.append(None)
            try:
                self.cursor.execute("UPDATE timetable SET subject1='%s', steart_time='%s', room_numb='%s' WHERE day_='Суббота' AND id='%s'", (row[2], row[4],row[3],row[0]))
                self.conn.commit()
            except:
                QMessageBox.about(self, "Error", "Enter all fields")

    def _create_monday_table(self):
        self.monday_table = QTableWidget()
        self.monday_table.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.monday_table.setColumnCount(4)
        self.monday_table.setHorizontalHeaderLabels(["Subject", "Time", "Room", ""])
        self._update_monday_table()
        self.mvbox = QVBoxLayout()
        self.mvbox.addWidget(self.monday_table)
        self.monday_gbox.setLayout(self.mvbox)




    def _update_shedule(self):
        self._update_monday_table()
        self._update_tuesday_table()
        self._update_wednesday_table()
        self._update_thursday_table()
        self._update_friday_table()
        self._update_saturday_table()

app = QApplication(sys.argv)
win = MainWindow()
win.show()
sys.exit(app.exec_())