'''
Дан словарь с названиями разных рептилий:
In [4]:
rept = {"python" : " питон",
 "anaconda" : "анаконда",
 "tortoize" : " черепаха" }
Добавьте в словарь пару "snake" - " змея".
Исправьте ключ "tortoize" на правильный "tortoise".
Выведите на экран сообщения вида
Питон по-английски будет python.
для всех слов в словаре (сообщение для каждого слова ‒ с новой строчки).
'''

def task1():
    di = {'python': ' питон',
          'anaconda': ' анаконда',
          'tortoize': ' черепаха'}
    di['snake'] = ' змея'
    di['tortoise'] = di['tortoize']
    del di['tortoize']

    for k in di:
        print(f'{di[k]} по-английски будет {k}')


'''
В списке cnt хранятся названия стран, а в списке fh – значения индекса Freedom House для этих
стран. Создайте словарь, используя в качестве ключей названия стран, а в качестве значений –
значения индекса.
In [5]:
cnt = ["Andorra", "Belarus", "Denmark",
 "Kenya", "Jamaica", "Romania"]
fh = [1.0, 6.0, 1.0, 4.0, 2.5, 2.0]
'''

def task2():
    cnt = ["Andorra", "Belarus", "Denmark", "Kenya", "Jamaica", "Romania"]
    fh = [1.0, 6.0, 1.0, 4.0, 2.5, 2.0]

    di = dict(zip(cnt, fh))
    print(di)


'''
Дан список, состоящий из пар чисел:
pairs = [(2, 4), (4, 6), (0, 1), (5, 2), (9, 1), (3, 8)]
Создайте словарь calc , где ключами являются пары чисел, а значениями – их произведение
(произведение тоже должно считаться в Python, не в уме).
'''

def task3():
    pairs = [(2, 4), (4, 6), (0, 1), (5, 2), (9, 1), (3, 8)]
    pairs_dict = {}
    for i in range(len(pairs)):
        for j in range(len(pairs)):
            pairs_dict[pairs[i][0],pairs[i][1]] = pairs[i][0] * pairs[i][1]
    print(pairs_dict)


'''
Дан словарь grades с оценками студентов за контрольную работу в 5-балльной шкале. Напишите код,
который сделает следующее:
Выведет на экран имя каждого студента и его оценку (каждый студент – с новой строки).
Сохранит имена студентов, получивших отличные оценки, в список excel .
Сохранит имена студентов, получивших хорошие оценки, в список good .
Сохранит имена студентов, получивших удовлетворительные оценки, в список satisf .
Сохранит имена студентов, получивших плохие оценки, в список bad .
grades = {'Anna': 4, 'Bob': 3, 'Claire': 5, 'Dick': 2, 'Elena': 5,
 'Fred': 5, 'George': 4, 'Kristina': 3, 'Nick': 2,
 'Ursula': 4, 'Viktor': 5}
'''


def task4():
    grades = {'Anna': 4, 'Bob': 3, 'Claire': 5, 'Dick': 2, 'Elena': 5,
          'Fred': 5, 'George': 4, 'Kristina': 3, 'Nick': 2,
          'Ursula': 4, 'Viktor': 5}
    excel = []
    good = []
    satisf = []
    bad = []
    for s in grades:
        print(f'{s} оценка {grades[s]}')
    for s in grades:
        if grades[s] == 5:
            excel.append(s)
        elif grades[s] == 4:
            good.append(s)
        elif grades[s] == 3:
            satisf.append(s)
        else:
            bad.append(s)
    print(f'5 получили {excel} \n'
        f'4 получили {good} \n'
        f'3 получили {satisf} \n'
        f'2 получили {bad}')
    

task1()
task2()
task3()
task4()