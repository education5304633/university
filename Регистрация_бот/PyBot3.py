import telebot
from telebot import types
import requests
import psycopg2

conn = psycopg2.connect(database="service_db",
                        user="postgres",
                        password="12341234",
                        host="localhost",
                        port="5432")
cursor = conn.cursor()

role_u = 'tg'

token = "5824149252:AAECJ3Q_lQcOBpNHYTC0whdmYNmYI6Uw1Eg"
bot = telebot.TeleBot(token)



@bot.message_handler(commands=['start'])
def start(message):
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row("Регистрация.")
    bot.send_message(message.chat.id, 'Привет! Я бот-регистратор. Я зарегистрирую тебя на пустом сайте!', reply_markup=keyboard)
    bot.register_next_step_handler(message, name_user);

@bot.message_handler(content_types=['text'])
def rega(message):
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row("Хочу ещё аккаунт.")
    bot.send_message(message.chat.id, 'Можно зарегистрировать ещё один аккаунт.', reply_markup=keyboard)
    bot.register_next_step_handler(message, name_user);

@bot.message_handler(content_types=['text'])
def name_user(message):
    bot.send_message(message.chat.id, 'Пожалуйста, введи имя.')
    bot.register_next_step_handler(message, send_text);

@bot.message_handler(content_types=['text'])
def send_text(message):
    global name_user
    name_user = message.text
    bot.send_message(message.chat.id, 'Теперь логин.')
    bot.register_next_step_handler(message, pass_1);

@bot.message_handler(content_types=['text'])
def pass_1(message):
    global login_user
    login_user = str(message.text)
    cursor.execute("SELECT EXISTS(SELECT id FROM service.users WHERE login = %s)", (login_user,))
    yn = cursor.fetchall()
    if str(yn[0][0]) == 'True':
        bot.send_message(message.chat.id, 'Такой логин уже есть, пожалуйста, введите корректный логин.')
        bot.register_next_step_handler(message, pass_1);
    else:
        bot.send_message(message.chat.id, 'Остался только пароль.')
        bot.register_next_step_handler(message, pass_f);

@bot.message_handler(content_types=['text'])
def pass_f(message):
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row("Продолжить")
    bot.send_message(message.chat.id, 'Регистрация завершена.', reply_markup=keyboard)
    global pass_user
    pass_user = message.text
    cursor.execute("INSERT INTO service.users (full_name, login, password, role) VALUES (%s, %s, %s, %s)", (name_user, login_user, pass_user, role_u))
    conn.commit()
    bot.register_next_step_handler(message, rega);


bot.polling()

