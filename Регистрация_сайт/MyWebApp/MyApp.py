from flask import Flask, render_template, request, redirect
import psycopg2

conn = psycopg2.connect \
        (
            database="service_db",
            user="postgres",
            password="12341234",
            host="localhost",
            port="5432"
        )
cur = conn.cursor()

app = Flask(__name__, template_folder="templates")

create_db = "CREATE TABLE IF NOT EXISTS users(id SERIAL PRIMARY KEY, full_name VARCHAR, password VARCHAR, login VARCHAR, role VARCHAR);"

cur.execute(create_db)

conn.commit()

admin = ('admin', 'admin', 'adm')

infa = "select * from users;"

cur.execute(infa)

db = cur.fetchall()

print(db)


@app.route('/register/', methods=['POST', 'GET'])
def register():
    global success
    success = ''
    usernames_exists = []
    if request.method == "POST":
        login = request.form.get("login")
        password = request.form.get("password")
        if login and password:
            cur.execute('SELECT login FROM users;')
            data_from_db = cur.fetchall()
            print(data_from_db)
            for username_from_db in data_from_db:
                usernames_exists.append(username_from_db[0])
            if login in usernames_exists:
                success = 'already_exists'
                print('already_exists')
            else:
                cur.execute(f"INSERT INTO users(login, password, role) VALUES ('{login}', '{password}',{0});")
                conn.commit()
                print(f'success saved {login, password}')
                success = 'account created'
        else:
            success = 'no username and password'
    return render_template('registration.html', success=success)


@app.route('/login/', methods=['GET', 'POST'])
def index():
    success = ''
    if request.method == 'POST':
        login = request.form.get("login")
        password = request.form.get("password")
        print(login, password)
        if login and password:
            cur.execute(f"SELECT * FROM users WHERE login='{login}';")
            data_from_db = cur.fetchone()
            if data_from_db == None:
                print('account not found')
                success = 'account not found'
            else:
                print(data_from_db)
                if data_from_db[1] == login and data_from_db[2] == password:
                    if data_from_db[4] == 1:
                        success = f'welcome to admin panel {login}'
                        return redirect(f'/admin/{success}')
                    success = 'signed in ' + str(data_from_db[0])
                    print(success)
                    return redirect(f'/account/{success}')

                else:
                    success = 'wrong pass'
                    print('wrong pass')
        else:
            print('no login or password')

    return render_template('login.html', success=success)


@app.route('/admin/<success>', methods=['GET', 'POST'])
def admin(success):
    return render_template('admin.html', success=success)


@app.route('/account/<success>', methods=['GET', 'POST'])
def main(success):
    return render_template('account.html', success=success)


app.run()
