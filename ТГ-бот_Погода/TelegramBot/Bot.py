import telebot
from telebot import types
import requests



token = "5730241356:AAF9AKFBYH6poE9qdALIZmHrWmcbEBFiNiU"
bot = telebot.TeleBot(token)


@bot.message_handler(commands=['start'])
def start(message):
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row("Погода", "/help")
    bot.send_message(message.chat.id, 'Привет! Я Погода-бот, я помогу тебе узнать погоду на сегодня или на неделю!',\
    reply_markup=keyboard)


@bot.message_handler(commands=['help'])
def start_message(message):
    bot.send_message(message.chat.id, 'Я умею выводить прогноз погоды на сегодня и на неделю.')





@bot.message_handler(content_types=['text'])
def send_text(message):
    if message.text == 'Погода':
            keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
            keyboard.row("На сегодня", "На неделю")
            bot.send_message(message.from_user.id, "Пожалуйста, выберете нужный пункт меню.", reply_markup=keyboard);
            bot.register_next_step_handler(message, city);
    else:
            bot.send_message(message.from_user.id, 'Я не понимаю, что Вы от меня хотите.');

def city(message):
    global tyty
    if message.text == 'На сегодня':
        tyty = 'tod'
        bot.send_message(message.chat.id, 'Пожалуйста, напишите название города.')
        bot.register_next_step_handler(message, type_);
    elif message.text == 'На неделю':
        tyty = 'week'
        bot.send_message(message.chat.id, 'Пожалуйста, напишите название города.')
        bot.register_next_step_handler(message, type_);
def type_(message):
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row("Назад")
    if tyty == 'tod':
        s_city = message.text
        appid = "2401b16e1c0cd93c9194efa68f094daf"
        res = requests.get("http://api.openweathermap.org/data/2.5/weather",
                           params={'q': s_city, 'units': 'metric', 'lang': 'ru', 'APPID': appid})
        data = res.json()
        bot.send_message(message.chat.id, f"Температура утром: {data['main']['temp_min']}°")
        bot.send_message(message.chat.id, f"Температура днем: {data['main']['temp_max']}°")
        bot.send_message(message.chat.id, f"Среднесуточная температура: {(data['main']['temp_min'] + data['main']['temp_max']) / 2}°")
        bot.send_message(message.chat.id, f"Влажность воздуха: {data['main']['humidity']}%")
        bot.send_message(message.chat.id, f"Атмосферное давление: {data['main']['pressure']} мм. рт. ст.")
        bot.send_message(message.chat.id, f"Скорость ветра: {data['wind']['speed']} м/с", reply_markup=keyboard)
    elif tyty == 'week':
        s_city = message.text
        appid = "2401b16e1c0cd93c9194efa68f094daf"
        res = requests.get("http://api.openweathermap.org/data/2.5/forecast",
                           params={'q': s_city, 'units': 'metric', 'lang': 'ru', 'APPID': appid})
        data = res.json()
        bot.send_message(message.chat.id, "Прогноз погоды на неделю:")
        for i in data['list']:
            bot.send_message(message.chat.id, f"Дата: {i['dt_txt']} \r\nТемпература: {'{0:+3.0f}'.format(i['main']['temp'])} \r\nПогодные условия:,\
    {i['weather'][0]['description']} \r\nВлажность воздуха: {i['main']['humidity']}% \r\nАтмосферное давление:\
    {i['main']['pressure']}  мм. рт. ст. \r\nСкорость ветра: {i['wind']['speed']} м/с")
            bot.send_message(message.chat.id, "____________________________", reply_markup=keyboard)
    bot.register_next_step_handler(message, start);

bot.polling()



