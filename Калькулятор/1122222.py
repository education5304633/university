from PyQt5 import QtCore, QtGui, QtWidgets
import math


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(360, 457)
        MainWindow.setStyleSheet("background-color: rgb(62, 62, 62);")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.result = QtWidgets.QLabel(self.centralwidget)
        self.result.setGeometry(QtCore.QRect(10, 10, 340, 71))
        self.result.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                                  "border-radius:20px;\n"
                                  "color: white;\n"
                                  "font-family: Arial;\n"
                                  "font-size: 28px;\n"
                                  "font-weight:600;\n"
                                  "")
        self.result.setObjectName("result")
        self.one = QtWidgets.QPushButton(self.centralwidget)
        self.one.setGeometry(QtCore.QRect(10, 160, 61, 61))
        self.one.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                               "border-radius:20px;\n"
                               "color: white;\n"
                               "font-family: Arial;\n"
                               "font-size: 28px;\n"
                               "font-weight:600")
        self.one.setObjectName("one")
        self.two = QtWidgets.QPushButton(self.centralwidget)
        self.two.setGeometry(QtCore.QRect(80, 160, 61, 61))
        self.two.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                               "border-radius:20px;\n"
                               "color: white;\n"
                               "font-family: Arial;\n"
                               "font-size: 28px;\n"
                               "font-weight:600")
        self.two.setObjectName("two")
        self.three = QtWidgets.QPushButton(self.centralwidget)
        self.three.setGeometry(QtCore.QRect(150, 160, 61, 61))
        self.three.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                                 "border-radius:20px;\n"
                                 "color: white;\n"
                                 "font-family: Arial;\n"
                                 "font-size: 28px;\n"
                                 "font-weight:600")
        self.three.setObjectName("three")
        self.plus = QtWidgets.QPushButton(self.centralwidget)
        self.plus.setGeometry(QtCore.QRect(220, 160, 61, 61))
        self.plus.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                                "border-radius:20px;\n"
                                "color: white;\n"
                                "font-family: Arial;\n"
                                "font-size: 28px;\n"
                                "font-weight:600")
        self.plus.setObjectName("plus")
        self.minus = QtWidgets.QPushButton(self.centralwidget)
        self.minus.setGeometry(QtCore.QRect(220, 230, 61, 61))
        self.minus.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                                 "border-radius:20px;\n"
                                 "color: white;\n"
                                 "font-family: Arial;\n"
                                 "font-size: 28px;\n"
                                 "font-weight:600")
        self.minus.setObjectName("minus")
        self.five = QtWidgets.QPushButton(self.centralwidget)
        self.five.setGeometry(QtCore.QRect(80, 230, 61, 61))
        self.five.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                                "border-radius:20px;\n"
                                "color: white;\n"
                                "font-family: Arial;\n"
                                "font-size: 28px;\n"
                                "font-weight:600")
        self.five.setObjectName("five")
        self.six = QtWidgets.QPushButton(self.centralwidget)
        self.six.setGeometry(QtCore.QRect(150, 230, 61, 61))
        self.six.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                               "border-radius:20px;\n"
                               "color: white;\n"
                               "font-family: Arial;\n"
                               "font-size: 28px;\n"
                               "font-weight:600")
        self.six.setObjectName("six")
        self.four = QtWidgets.QPushButton(self.centralwidget)
        self.four.setGeometry(QtCore.QRect(10, 230, 61, 61))
        self.four.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                                "border-radius:20px;\n"
                                "color: white;\n"
                                "font-family: Arial;\n"
                                "font-size: 28px;\n"
                                "font-weight:600")
        self.four.setObjectName("four")
        self.multiply = QtWidgets.QPushButton(self.centralwidget)
        self.multiply.setGeometry(QtCore.QRect(220, 300, 61, 61))
        self.multiply.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                                    "border-radius:20px;\n"
                                    "color: white;\n"
                                    "font-family: Arial;\n"
                                    "font-size: 28px;\n"
                                    "font-weight:600")
        self.multiply.setObjectName("multiply")
        self.eight = QtWidgets.QPushButton(self.centralwidget)
        self.eight.setGeometry(QtCore.QRect(80, 300, 61, 61))
        self.eight.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                                 "border-radius:20px;\n"
                                 "color: white;\n"
                                 "font-family: Arial;\n"
                                 "font-size: 28px;\n"
                                 "font-weight:600")
        self.eight.setObjectName("eight")
        self.nine = QtWidgets.QPushButton(self.centralwidget)
        self.nine.setGeometry(QtCore.QRect(150, 300, 61, 61))
        self.nine.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                                "border-radius:20px;\n"
                                "color: white;\n"
                                "font-family: Arial;\n"
                                "font-size: 28px;\n"
                                "font-weight:600")
        self.nine.setObjectName("nine")
        self.seven = QtWidgets.QPushButton(self.centralwidget)
        self.seven.setGeometry(QtCore.QRect(10, 300, 61, 61))
        self.seven.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                                 "border-radius:20px;\n"
                                 "color: white;\n"
                                 "font-family: Arial;\n"
                                 "font-size: 28px;\n"
                                 "font-weight:600")
        self.seven.setObjectName("seven")
        self.zero = QtWidgets.QPushButton(self.centralwidget)
        self.zero.setGeometry(QtCore.QRect(10, 370, 61, 61))
        self.zero.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                                "border-radius:20px;\n"
                                "color: white;\n"
                                "font-family: Arial;\n"
                                "font-size: 28px;\n"
                                "font-weight:600")
        self.zero.setObjectName("zero")
        self.equally = QtWidgets.QPushButton(self.centralwidget)
        self.equally.setGeometry(QtCore.QRect(150, 370, 61, 61))
        self.equally.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                                   "border-radius:20px;\n"
                                   "color: white;\n"
                                   "font-family: Arial;\n"
                                   "font-size: 28px;\n"
                                   "font-weight:600")
        self.equally.setObjectName("equally")
        self.split = QtWidgets.QPushButton(self.centralwidget)
        self.split.setGeometry(QtCore.QRect(220, 370, 61, 61))
        self.split.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                                 "border-radius:20px;\n"
                                 "color: white;\n"
                                 "font-family: Arial;\n"
                                 "font-size: 28px;\n"
                                 "font-weight:600")
        self.split.setObjectName("split")
        self.cansel = QtWidgets.QPushButton(self.centralwidget)
        self.cansel.setGeometry(QtCore.QRect(80, 370, 61, 61))
        self.cansel.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                                  "border-radius:20px;\n"
                                  "color: white;\n"
                                  "font-family: Arial;\n"
                                  "font-size: 28px;\n"
                                  "font-weight:600")
        self.cansel.setObjectName("cansel")
        self.bracket_close = QtWidgets.QPushButton(self.centralwidget)
        self.bracket_close.setGeometry(QtCore.QRect(80, 90, 61, 61))
        self.bracket_close.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                                         "border-radius:20px;\n"
                                         "color: white;\n"
                                         "font-family: Arial;\n"
                                         "font-size: 28px;\n"
                                         "font-weight:600")
        self.bracket_close.setObjectName("bracket_close")
        self.bracket_open = QtWidgets.QPushButton(self.centralwidget)
        self.bracket_open.setGeometry(QtCore.QRect(10, 90, 61, 61))
        self.bracket_open.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                                        "border-radius:20px;\n"
                                        "color: white;\n"
                                        "font-family: Arial;\n"
                                        "font-size: 28px;\n"
                                        "font-weight:600")
        self.bracket_open.setObjectName("bracket_open")
        self.dot = QtWidgets.QPushButton(self.centralwidget)
        self.dot.setGeometry(QtCore.QRect(220, 90, 61, 61))
        self.dot.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                               "border-radius:20px;\n"
                               "color: white;\n"
                               "font-family: Arial;\n"
                               "font-size: 28px;\n"
                               "font-weight:600")
        self.dot.setObjectName("dot")
        self.exp = QtWidgets.QPushButton(self.centralwidget)
        self.exp.setGeometry(QtCore.QRect(150, 90, 61, 61))
        self.exp.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                               "border-radius:20px;\n"
                               "color: white;\n"
                               "font-family: Arial;\n"
                               "font-size: 28px;\n"
                               "font-weight:600")
        self.exp.setObjectName("exp")
        self.cos = QtWidgets.QPushButton(self.centralwidget)
        self.cos.setGeometry(QtCore.QRect(290, 160, 61, 61))
        self.cos.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                               "border-radius:20px;\n"
                               "color: white;\n"
                               "font-family: Arial;\n"
                               "font-size: 28px;\n"
                               "font-weight:600")
        self.cos.setObjectName("cos")
        self.pi = QtWidgets.QPushButton(self.centralwidget)
        self.pi.setGeometry(QtCore.QRect(290, 370, 61, 61))
        self.pi.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                              "border-radius:20px;\n"
                              "color: white;\n"
                              "font-family: Arial;\n"
                              "font-size: 28px;\n"
                              "font-weight:600")
        self.pi.setObjectName("pi")
        self.sin = QtWidgets.QPushButton(self.centralwidget)
        self.sin.setGeometry(QtCore.QRect(290, 90, 61, 61))
        self.sin.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                               "border-radius:20px;\n"
                               "color: white;\n"
                               "font-family: Arial;\n"
                               "font-size: 28px;\n"
                               "font-weight:600")
        self.sin.setObjectName("sin")
        self.tg = QtWidgets.QPushButton(self.centralwidget)
        self.tg.setGeometry(QtCore.QRect(290, 230, 61, 61))
        self.tg.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                              "border-radius:20px;\n"
                              "color: white;\n"
                              "font-family: Arial;\n"
                              "font-size: 28px;\n"
                              "font-weight:600")
        self.tg.setObjectName("tg")
        self.cat = QtWidgets.QPushButton(self.centralwidget)
        self.cat.setGeometry(QtCore.QRect(290, 300, 61, 61))
        self.cat.setStyleSheet("background-color: rgb(52, 59, 71);\n"
                               "border-radius:20px;\n"
                               "color: white;\n"
                               "font-family: Arial;\n"
                               "font-size: 28px;\n"
                               "font-weight:600")
        self.cat.setObjectName("cat")

        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.add_functions()

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.one.setText(_translate("MainWindow", "1"))
        self.two.setText(_translate("MainWindow", "2"))
        self.three.setText(_translate("MainWindow", "3"))
        self.plus.setText(_translate("MainWindow", "+"))
        self.minus.setText(_translate("MainWindow", "-"))
        self.five.setText(_translate("MainWindow", "5"))
        self.six.setText(_translate("MainWindow", "6"))
        self.four.setText(_translate("MainWindow", "4"))
        self.multiply.setText(_translate("MainWindow", "*"))
        self.eight.setText(_translate("MainWindow", "8"))
        self.nine.setText(_translate("MainWindow", "9"))
        self.seven.setText(_translate("MainWindow", "7"))
        self.zero.setText(_translate("MainWindow", "0"))
        self.equally.setText(_translate("MainWindow", "="))
        self.split.setText(_translate("MainWindow", "/"))
        self.cansel.setText(_translate("MainWindow", "C"))
        self.bracket_close.setText(_translate("MainWindow", ")"))
        self.bracket_open.setText(_translate("MainWindow", "("))
        self.dot.setText(_translate("MainWindow", "."))
        self.exp.setText(_translate("MainWindow", "e"))
        self.sin.setText(_translate("MainWindow", "sin"))
        self.cos.setText(_translate("MainWindow", "cos"))
        self.tg.setText(_translate("MainWindow", "tg"))
        self.cat.setText(_translate("MainWindow", "cat"))
        self.pi.setText(_translate("MainWindow", "sqrt"))


    def add_functions(self):
        self.zero.clicked.connect(lambda: self.write_number(self.zero.text()))
        self.one.clicked.connect(lambda: self.write_number(self.one.text()))
        self.two.clicked.connect(lambda: self.write_number(self.two.text()))
        self.three.clicked.connect(lambda: self.write_number(self.three.text()))
        self.four.clicked.connect(lambda: self.write_number(self.four.text()))
        self.five.clicked.connect(lambda: self.write_number(self.five.text()))
        self.six.clicked.connect(lambda: self.write_number(self.six.text()))
        self.seven.clicked.connect(lambda: self.write_number(self.seven.text()))
        self.eight.clicked.connect(lambda: self.write_number(self.eight.text()))
        self.nine.clicked.connect(lambda: self.write_number(self.nine.text()))
        self.plus.clicked.connect(lambda: self.write_number(self.plus.text()))
        self.minus.clicked.connect(lambda: self.write_number(self.minus.text()))
        self.split.clicked.connect(lambda: self.write_number(self.split.text()))
        self.multiply.clicked.connect(lambda: self.write_number(self.multiply.text()))
        self.bracket_open.clicked.connect(lambda: self.write_number(self.bracket_open.text()))
        self.bracket_close.clicked.connect(lambda: self.write_number(self.bracket_close.text()))
        self.dot.clicked.connect(lambda: self.write_number(self.dot.text()))
        self.exp.clicked.connect(lambda: self.write_number(self.exp.text()))

        self.sin.clicked.connect(self.sin_f)
        self.cos.clicked.connect(self.cos_f)
        self.tg.clicked.connect(self.tg_f)
        self.cat.clicked.connect(self.cat_f)
        self.pi.clicked.connect(self.sqrt_f)
        self.cansel.clicked.connect(self.cansel_results)
        self.equally.clicked.connect(self.results)


    def sin_f(self):
        self.result.setText(str(math.sin(float(self.result.text()))))


    def cos_f(self):
        self.result.setText(str(math.cos(float(self.result.text()))))

    def tg_f(self):
        self.result.setText(str(math.tan(float(self.result.text()))))

    def cat_f(self):
        self.result.setText(str(1/math.tan(float(self.result.text()))))

    def sqrt_f(self):
        try:
            self.result.setText(str(math.sqrt(float(self.result.text()))))
        except:
            self.result.setText('Чел ты...')


    def cansel_results(self):
        self.result.setText('0')

    def write_number(self, number):
        if self.result.text() == '0' or self.result.text() == 'Error':
            self.result.setText(number)
        else:
            self.result.setText(self.result.text()+number)

    def results(self):
        try:
            res = str(self.result.text())
            if "e" in res:
                res1 = res.replace("e", "2.71828")
            else:
                res1 = res
            resf = str(eval(res1))
            self.result.setText(resf)
        except:
            self.result.setText('Error')

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
